//
//  ViewController.swift
//  eCommerce
//
//  Created by Ajeet N on 24/01/19.
//  Copyright © 2019 Ajeet N. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var userNameObj: UITextField!
    @IBOutlet var passwordObj: UITextField!
    @IBOutlet var signupButtonObj: UIButton!
    @IBOutlet var dontHaveAccountObj: UIButton!
    @IBOutlet var activityIndicatorObj: UIActivityIndicatorView!
    @IBAction func signInAction(_ sender: UIButton) {
    }
    @IBAction func forgetPasswordAction(_ sender: UIButton) {
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }


}

